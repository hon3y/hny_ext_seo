<?php

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

// <link rel="alternate" hreflang="x" href="http://x.beispiel.com" />

// rel
// hreflang (language iso)
// href alternative page/url

$aTempColumns = array(
    'tx_hiveextseo_1' => array(
        'exclude' => 0,
        'label' => 'LLL:EXT:hive_ext_seo/Resources/Private/Language/Backend.xlf:pages.tx_hiveextseo_1',
        'config' => array(
            'type' => 'group',
            'internal_type' => 'db',
            'allowed' => 'pages',
            'foreign_table' => 'pages',
            'MM' => 'tx_hiveextseo_page_page_mm',
            'MM_match_fields' => array(
                'tablenames' => 'pages',
            ),
            'MM_insert_fields' => array(
                'tablenames' => 'pages',
            ),
            'size' => 6,
            'autoSizeMax' => 30,
            'minitems' => 0,
            'maxitems' => 9999,
            'selectedListStyle' => 'width:400px;',
            'wizards' => array(
                '_PADDING' => 0,
                '_VERTICAL' => 0,
                'suggest' => array(
                    'type' => 'suggest',
                    'pages' => array(
                        'searchWholePhrase' => 1,
                    ),
                ),
            ),
        ),
    ),

    'tx_hiveextseo_2' => array(
        'exclude' => 0,
        'label' => 'LLL:EXT:hive_ext_seo/Resources/Private/Language/Backend.xlf:pages.tx_hiveextseo_2',
        'config' => array(
            'type' => 'group',
            'internal_type' => 'db',
            'allowed' => 'pages',
            'foreign_table' => 'pages',
            'MM' => 'tx_hiveextseo_page_page_mm',
            'MM_match_fields' => array(
                'tablenames' => 'pages',
            ),
            'MM_insert_fields' => array(
                'tablenames' => 'pages',
            ),
            'MM_opposite_field' => 'tx_hiveextseo_1',
            'size' => 6,
            'autoSizeMax' => 30,
            'minitems' => 0,
            'maxitems' => 9999,
            'selectedListStyle' => 'width:400px;',
            'disable_controls' => 'browser',
        ),
    ),

    'tx_hiveextseo_list' => array(
        'exclude' => 0,
        'label' => 'LLL:EXT:hive_ext_seo/Resources/Private/Language/Backend.xlf:pages.tx_hiveextseo_list',
        'config' => array(
            'type' => 'user',
            'userFunc' => 'HIVE\HiveExtSeo\Utility\HreflangTags->renderBackendList',
        ),
    ),
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $aTempColumns, 1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('pages', 'tx_bgmhreflang', 'tx_hiveextseo_1,--linebreak--,tx_hiveextseo_2,--linebreak--,tx_hiveextseo_list');
$GLOBALS['TCA']['pages']['palettes']['tx_hiveextseo']['canNotCollapse'] = 1;
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages', '--palette--;LLL:EXT:hive_ext_seo/Resources/Private/Language/Backend.xlf:pages.palette.tx_hiveextseo;tx_hiveextseo;;', '', 'after:lastUpdated');

$GLOBALS['TCA']['pages']['ctrl']['setToDefaultOnCopy'] = ($GLOBALS['TCA']['pages']['ctrl']['setToDefaultOnCopy'] ? $GLOBALS['TCA']['pages']['ctrl']['setToDefaultOnCopy'] . ',' : '') . 'tx_hiveextseo_1,tx_hiveextseo_2';